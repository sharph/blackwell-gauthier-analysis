## Some small analysis on Blackwell vs Gauthier campaign finance

*May 7, 2019*

With campaign finance an issue in the 2019 election for councilperson for Philadelphia's 3rd district, I thought I would list and sort sources of funding for both candidates for both 2018 + 2019 to see if there is anything that can be learned. This information is from the public record at [https://apps.phila.gov/campaign-finance/search/all](https://apps.phila.gov/campaign-finance/search/all), but after processing the listing I generated sums donations from the same donor, rather than listing them separately.

These data can't account for non-cash support from entities, for example the flyers and door hangers from Philadelphia 3.0 or flyers of undisclosed origin in support of the Blackwell campaign.

If you have comments about this presentation of data or otherwise want to get in touch, at [sharp@sharphall.org](mailto:sharp@sharphall.org).

In full disclosure, I am a Gauthier supporter and have donated to her campaign.

## Process

* I wrote [a python script](https://gitlab.com/sharph/blackwell-gauthier-analysis/blob/master/analyze.py) to parse exported data from [https://apps.phila.gov/campaign-finance/search/all](https://apps.phila.gov/campaign-finance/search/all) to sort donors for each candidate by total contribution, and also generated binned counts of donors by total donation size.
* The 2019 CSV produced by Philadelphia's site has many errors due to commas not being escaped. (2018 uses tabs instead of commas. As far as machine readability it was totally fine.) I had to write my script to raise an error when input was not in the correct format so I could find the erroneous lines and correct them.
* Once I was generating output, I noticed a couple of records that should have been summed together but were not because of a truncation error or because a word in one address line was abbreviated in one record but not in another. I wrote code into my script to find these records and associate them on the fly, instead of changing the data files. The files were only altered to assist with parsing badly formed comma separated fields, as above. I was still seeing some of these until recently. If you catch one I haven't, email [sharp@sharphall.org](mailto:sharp@sharphall.org).

## Initial observations

* Jamie Gauthier made a donation to her own campaign, as did some relatives or family-members (presumably) who share the Gauthier name. The name Blackwell does not appear in Jannie Blackwell's list of donors.
* Blackwell's top funders: unions and real estate.
* Gauthier's top funders: an environmental conservation organization and venture capitalists; yes, Perelman is in there too. Those accused of making dark in-kind donations to Gauthier through Philadelphia 3.0 are also here in plain sight.
* Gauthier has a strong base of small individual donors where Blackwell does not.
* Address may in some cases be misleading for determining residency, for example in the case of Gauthier supporters Josh and Rena Kopelman who used "4040 Locust St," the address of First Round Capital of which Josh is a partner. I have a hunch this is more of an issue where there is big money.
* Including 2018 data here is charitable to Blackwell. A very large majority of her smaller donors donated in 2018, not 2019.

## Next steps

* Re-running the numbers summing per *household* instead of *donor name* could be interesting.
* Donor counts could be visualized in a histogram.
* A [dot density map](http://wiki.gis.com/wiki/index.php/Dot_density_map) could be illustrative for visualizing from where in the city financial support is coming from. $50 seems to be a nice round number with this data, so 1 dot = $50 could work nicely.

## Donor counts by size of total donation
|Amount|Blackwell|$|Gauthier|$|
|:--|:--|:--|:--|:--|
|$0.00-$200.00|26|$2840.00|232|$27845.94|
|$200.01-$400.00|37|$9400.00|66|$18070.00|
|$400.01-$800.00|29|$14650.00|33|$16950.00|
|$800.01-$1600.00|32|$32900.00|11|$11250.00|
|$1600.01-$3200.00|17|$41300.00|11|$32000.00|
|$3200.01-$6400.00|3|$11763.92|6|$32500.00|
|$6400.01-$12800.00|3|$36300.00|1|$10000.00|
|Total|147|$149153.92|360|$148615.94|

## Jannie Blackwell

147 donors contributing $149153.92 in total in 2018 + 2019

|Amount|Name|City, Zip|
|:--|:--|:--|
|$12500.00|Sheet Metal Workers Union Local 19|Philadelphia, PA 19147|
|$11900.00|LABORERS DISTRICT COUNCIL (PAC)|PHILA, PA 19123|
|$11900.00|LOCAL UNION 98 IBEW|PHILA, PA 19130|
|$5000.00|Robert Nicoletti|King of Prussia, PA 19406|
|$3500.00|GRAYS & GRAYS LLC|PHILA, PA 19143|
|$3263.92|Stanley Straughter|Philadelphia, PA 19126|
|$3000.00|Swanson Street Associates|Blue Bell, PA 19422|
|$2900.00|CRAIG SABATINO|WALLINGFORD, PA 19086|
|$2900.00|PLUMBERS UNION LOCAL 690|PHILA, PA 19155|
|$2500.00|RAMY SHRAIM|PHILA, PA 19130|
|$2500.00|PFT Committee to Support Public Education|PHILA, PA 19103|
|$2500.00|ISMET AJUAZI|Philadelphia, PA 19125|
|$2500.00|Craig J. Sabatino|Philadelphia, PA 19104|
|$2500.00|BRAHIM IGHLADEN|PHILA, PA 19104|
|$2500.00|HAVERFORD SQUARE, G.G., LLC|PHILA, PA 19147|
|$2500.00|Laborers Dictrict Council Pac|Philadelphia, PA 19123|
|$2500.00|Steam Fitter Local Union 420|Philadelphia, PA 19154|
|$2500.00|FREDRICO L. ALMAS|PHILA, PA 19104|
|$2000.00|DARRELL CHOATES|GLASSBORO, NJ 08028|
|$2000.00|SWANSON STREET ASSOCIATES|BLUE BELL, PA 19422|
|$2000.00|DOMINICK A. CIPOLLINI|CHELTENHAM, PA 19012|
|$2000.00|GLENN A. FALSO, JR.|BLUE BELL, PA 19422|
|$2000.00|John A. Fry|Haverford, PA 19041|
|$1500.00|EUGENE NAYDOVICH|PHILA, PA 19103|
|$1500.00|SIDNEY BOOKER|LAVERROCK, PA 19038|
|$1000.00|PECO PAC|Philadelphia, PA 19106|
|$1000.00|Dr. L. H. Brown|Philadelphia, PA 19106|
|$1000.00|Duane Morris LLP Government|Philadelphia, PA 19103|
|$1000.00|Greenwalt|Wyndmor, PA 19038|
|$1000.00|JAMIE ROBERTSON|DREXEL HILL, PA 19026|
|$1000.00|KRISTOPHER WOOD|PLYMOUTH MEETING, PA 19402|
|$1000.00|EVELYN HIDALGO|HAVERTOWN, PA 19083|
|$1000.00|PFT Committee to Support Public Education|Philadelphia, PA 19103|
|$1000.00|IRON WORKERS LOCAL 401 PAC|PHILA, PA 19154|
|$1000.00|PETER LONGSTRETH|PHILA, PA 19118|
|$1000.00|JAMES EGAN|CHURCHVILLE, PA 18966|
|$1000.00|GREENWALT|WYNMOOR, PA 19038|
|$1000.00|Duane Morris LLP|Philadelphia, PA 19103|
|$1000.00|ERDEY MCHENRY ARCH, LLC|Philadelphia, PA 19123|
|$1000.00|DILWORTH PAXON|PHILA, PA 19102|
|$1000.00|JAMES EGAN|CHURCHVILLE, PA 18966|
|$1000.00|RAFIYA SEARS|Philadelphia, PA 19130|
|$1000.00|LORENA POOLE-NARAUJO|PHILA, PA 19118|
|$1000.00|C. EDWARD HILLIS|PHILA, PA 19144|
|$1000.00|DUANE MORRIS LLP|PHILA, PA 19103|
|$1000.00|GARY A JONAS, JR.|PLYMOUTH, PA 19402|
|$1000.00|Iron Workers Local No. 405|Philadelphia, PA 19146|
|$1000.00|C. Edward Hillis|Philadelphia, PA 19144|
|$1000.00|Peter Longstreth|Philadelphia, PA 19102|
|$1000.00|Saul Ewing LLP|Philadelphia, PA 19102|
|$1000.00|Dr. L. H. Brown|Philadelphia, PA 19106|
|$1000.00|NICHOLAS DEBENEDICTIS|ARDMORE, PA 19003|
|$1000.00|PHILA BUILDERS ASSOC,|PHILA, PA 19103|
|$1000.00|KETH B KEY|POWELL, OH 43065|
|$900.00|ABDOUL WANE|PHILA, PA 19139|
|$650.00|Lloyd C. Murray|Philadelphia, PA 19139|
|$500.00|DAVID KRAIN|PHILA, PA 19114|
|$500.00|CURTIS M. HESS|GLEN MILLS, PA 19342|
|$500.00|DONNA ALLIE|PHILA, PA 19112|
|$500.00|Gary Risler|Philadelphia, PA 19147|
|$500.00|PECO PAC|PHILA, PA 19103|
|$500.00|Transport Workers Union #734|Philadelphia, PA 19123|
|$500.00|VAUGHN COOK|PHILA, PA 19130|
|$500.00|STANLEY SILVERMAN|DRESCHER, PA 19025|
|$500.00|JETTA MILLER|Philadelphia, PA 19148|
|$500.00|TEDLA ABRAHAM|Philadelphia, PA 19104|
|$500.00|Oshunbumi Fernandez|Philadelphia, PA 19146|
|$500.00|JAMES NIXON|Philadelphia, PA 19131|
|$500.00|TILAHUN W. DEGEFU|WALLINGFORD, PA 19086|
|$500.00|JOANN BELL|Philadelphia, PA 19129|
|$500.00|Anthony B. Crawley|Philadelphia, PA 19106|
|$500.00|ANDREW BLUM|MALVERN, PA 19355|
|$500.00|Tom Lussenhop|Philadelphia, PA 19104|
|$500.00|TSANDI WILLIAMS|CHELTENHAM, PA 19012|
|$500.00|JOSEPH REAGAN JR.|PHILA, PA 19103|
|$500.00|CURTIS HESS|DREXEL HILL, PA 19026|
|$500.00|1305 N. 27TH, LLC|HAVERFORD, PA 19041|
|$500.00|SHAWN NAM|VOORHEES, NJ 08043|
|$500.00|Lucy E. Kerman|Bala Cynwyd, PA 19004|
|$500.00|GEORGE A. REYNOLDS|EAST POINT, GA 30344|
|$500.00|WILLIAM WILSON|Philadelphia, PA 19112|
|$500.00|MOLEFI K. ASANTE|PHILA, PA 19144|
|$500.00|William J. McLaughlin|Spring City, PA 19475|
|$500.00|JERRY L. COTTOU|EVESHAM, NJ 08053|
|$300.00|BERHANE AKLILU|GALLOWAY, NJ 08205|
|$300.00|TIMOTHY ARIZIN|KING OF PRUSSIA, PA 19406|
|$300.00|WILLIAM J. EDELSTEIN|NEWRON, PA 18940|
|$250.00|CAROL LABOR|WESTMONT, NJ 08108|
|$250.00|THOMAS MUNOLYN|PENNSAUKEN, NJ 08110|
|$250.00|Zerihun T Belay|Sicklerville, NJ 08081|
|$250.00|AL HOPKINS|NORTH WALES, PA 19454|
|$250.00|LOWELL LEE THOMAS|PHILA, PA 19119|
|$250.00|TYRONE HART|PHILA, PA 19147|
|$250.00|Carolina B. Harris|Philadelphia, PA 19143|
|$250.00|MALIK MAJEED|MOORESTOWN, NJ 08057|
|$250.00|AINME TCHAPDA|Philadelphia, PA 19114|
|$250.00|WALTER MCGILL|NEWARK, DE 19072|
|$250.00|HAYACINTH ANYIAM|WILLIAMSTOWN, NJ 08094|
|$250.00|SADDIQ ABESHERA|Philadelphia, PA 19139|
|$250.00|ELI KLEHR|NEW YORK, NY 10021|
|$250.00|GRIMAYE JIRA|Philadelphia, PA 19082|
|$250.00|MICHAEL HORSEY|WYNCOTE, PA 19095|
|$250.00|JAMAL M. OSMIN|Philadelphia, PA 19131|
|$250.00|MAJORY REAGAN|PHILA, PA 19103|
|$250.00|JOSEPH STORCK|MARLTON, NJ 08053|
|$250.00|LATONYA SAMUELS|FEASTERVILLE, PA 19053|
|$250.00|CARL M. BUCHHOLZ|FLOURTOWN, PA 19320|
|$250.00|HABTAMU KASSU|Philadelphia, PA 19142|
|$250.00|MAURICE BRASWELL|WYNCOTE, PA 19095|
|$250.00|FRANK M. SAHON|BEAR, DE 19705|
|$250.00|James C. Dobrowolski|Columbus, NJ 08022|
|$250.00|ELVIN ROSS|Philadelphia, PA 19129|
|$250.00|CAROL BANGUARA|HADDON TOWNSHIP, NJ 08108|
|$250.00|ALMAZ SAUD|PHILA, PA 19143|
|$250.00|HABTAMU SHITAYE|Philadelphia, PA 19143|
|$250.00|ALAN F. MOHLSTETTER|GLENSIDE, PA 19038|
|$250.00|WOUBSHET GEBREAMAUUEL|Philadelphia, PA 19131|
|$250.00|ASTER BELETEV|YEADON, PA 19057|
|$250.00|DEMELOZ DEMEME|Philadelphia, PA 19143|
|$250.00|TIMOTHY ARIZIN|KING OF PRUSSIA, PA 19406|
|$250.00|HYACENTH ANYIAM|WILLIAMSTOWN, NJ 08094|
|$200.00|SAMUEL MURPHY|Philadelphia, PA 19125|
|$200.00|Blane F. Stoddart|Philadelphia, PA 19106|
|$200.00|REGINALD FULLER|CAMDEN, NJ 08102|
|$200.00|DOLORES MUHAMMED|PHILA, PA 19151|
|$200.00|MARTIN JOSEPH ONZIK|HOLLAND, PA 18966|
|$150.00|Gaston Mbonglou|NORRISTOWN, PA 19403|
|$150.00|Volfe Jabateh|Philadelphia, PA 19143|
|$100.00|Dr. Samuel Quartey|Vorhees, NJ 08043|
|$100.00|DIL A. KULATHUM|HARLEYSVILLE, PA 19638|
|$100.00|LORETT S. JEMMOTT|PHILA, PA 19147|
|$100.00|TAHIYA NYAHUMA|PHILA, PA 19132|
|$100.00|WILLIAM S. WAYNE|Philadelphia, PA 19146|
|$100.00|KAREN LEWIS|PHILA, PA 19119|
|$100.00|NAOMI JOHNSON BOOKER|DRESHER, PA 19025|
|$100.00|MARTIN J. ONZIK|HOLLAND, PA 18966|
|$100.00|MICHAEL A. GRIFTON|Philadelphia, PA 19121|
|$100.00|SHALADA SANIKAFA|CHESTER, PA 19013|
|$100.00|GWENDOLYN MORRIS|PHILA, PA 19104|
|$100.00|JOHN KRAUSE|ARDMORE, PA 19003|
|$50.00|DOROTHY ALEXANDER|GIBBSBORO, NJ 08026|
|$50.00|KEVIN WILLIAMS|Philadelphia, PA 19121|
|$50.00|ROBERTA ALFORD|Philadelphia, PA 19103|
|$50.00|FSIDORA OGUIOSSAN|Philadelphia, PA 19151|
|$50.00|PAT SANFORD|Philadelphia, PA 19151|
|$50.00|ADRIENNE MCKINNEY|Philadelphia, PA 19147|
|$40.00|VICTORIA SEMOME|DARBY, PA 19023|


## Jamie Gauthier

360 donors contributing $148615.94 in total in 2018 + 2019


|Amount|Name|City, Zip|
|:--|:--|:--|
|$10000.00|Conservation Voters of Pennsylvania|Philadelphia, PA 19102|
|$6000.00|Josh Verne|Gladwyne, PA 19035|
|$6000.00|Stephen Herman|Philadelphia, PA 19104|
|$6000.00|Josh Kopelman|Philadelphia, PA 19104|
|$6000.00|Rena Kopelman|Philadelphia, PA 19104|
|$5000.00|Marion Layton Mann|Bryn Mawr, PA 19010|
|$3500.00|Brian Selander|Chadds Ford, PA 19137|
|$3000.00|Dalia Herman|Philadelphia, PA 19104|
|$3000.00|Richard Vague|Philadelphia, PA 19103|
|$3000.00|Richard Wade Vague|Philadelphia, PA 19103|
|$3000.00|Janet F Haas|Wayne, PA 19087|
|$3000.00|Kami Verne|Gladwyne, PA 19035|
|$3000.00|Marsha Perelman|Wynnewood, PA 19096|
|$3000.00|Bonny A Ault|Philadelphia, PA 19137|
|$3000.00|Laura Vague|Philadelphia, PA 19103|
|$3000.00|Richard Green|Bryn Mawr, PA 19010|
|$2500.00|Judy Williams|Philadelphia, PA 19131|
|$2500.00|Second Generation|Philadelphia, PA 19106|
|$1250.00|Charles Nygard|Philadelphia, PA 19106|
|$1000.00|Bonny Ault|Philadelphia, PA 19137|
|$1000.00|Andrea Stewart|Bryn Mawr, PA 19010|
|$1000.00|Laura Kind McKenna|Wyndmoor, PA 19038|
|$1000.00|Joseph H Kluger|Gladwyne, PA 19035|
|$1000.00|Carl Battle|Sarasota, FL 34240|
|$1000.00|5th Square|Philadelphia, PA 19146|
|$1000.00|Julia Hillengas|Philadelphia, PA 19143|
|$1000.00|Ken Weinstein|Philadelphia, PA 19119|
|$1000.00|Barbara Capozzi|Philadelphia, PA 19145|
|$1000.00|Michael Heller|Villanova, PA 19085|
|$750.00|Susan Firestone|Yardley, PA 19067|
|$600.00|Angela H Coghlan|Philadelphia, PA 19143|
|$600.00|Stephen Huntington|Philadelphia, PA 19103|
|$500.00|Max Berger|Narberth, PA 19072|
|$500.00|Richard Green|Conshohocken, PA 19428|
|$500.00|Erica Atwood|Philadelphia, PA 19143|
|$500.00|Barry Abelson|Bryn Mawr, PA 19010|
|$500.00|Leslie Benoliel|Philadelphia, PA 19119|
|$500.00|Lou Rodriguez|Williamstown, NJ 08094|
|$500.00|Amara Rockar|Philadelphia, PA 19143|
|$500.00|Thomas J Knox|Philadelphia, PA 19102|
|$500.00|David M Seltzer|Philadelphia, PA 19106|
|$500.00|Elsa Wilson|Magnolia, DE 19962|
|$500.00|Natalie D Goldberg|Philadelphia, PA 19118|
|$500.00|Judy Wicks|Philadelphia, PA 19103|
|$500.00|Andrew Stober|Philadelphia, PA 19147|
|$500.00|Bruce Marks|Philadelphia, PA 19130|
|$500.00|John D Lindsay|Philadelphia, PA 19104|
|$500.00|David A Krupnick|Philadelphia, PA 19147|
|$500.00|Colleen Bracken|Philadelphia, PA 19144|
|$500.00|Brian Effron|Bryn Mawr, PA 19010|
|$500.00|Thomas Gravina|Haverford, PA 19041|
|$500.00|Beulah Trey|Philadelphia, PA 19119|
|$500.00|Andrew Yaffe|Philadelphia, PA 19103|
|$500.00|Roger Braunfeld|Penn Valley, PA 19072|
|$500.00|David Grasso|Philadelphia, PA 19102|
|$500.00|Nathan Boon|Philadelphia, PA 19146|
|$500.00|David M Tilley|Philadelphia, PA 19104|
|$500.00|Michael Hagan|Newtown, PA 18940|
|$500.00|Drick Boyd|Broomall, PA 19008|
|$500.00|Christopher Cramer|Philadelphia, PA 19148|
|$500.00|Matthew Faranda-Diedrich|Wayne, PA 19087|
|$500.00|Ellen Balze|Philadelphia, PA 19143|
|$350.00|Michael A Bloom|Philadelphia, PA 19106|
|$350.00|Alex Doty|Philadelphia, PA 19143|
|$350.00|Amara Moreau Briggs|Bala Cynwyd, PA 19004|
|$350.00|Saleem Chapman|Philadelphia, PA 19139|
|$350.00|Kate Logan|Philadelphia, PA 19104|
|$350.00|Zita James|Yeadon, PA 19050|
|$350.00|Nicholas Wood|Philadelphia, PA 19139|
|$350.00|Alan Greenberger|Philadelphia, PA 19119|
|$350.00|Martha Ledger|Philadelphia, PA 19143|
|$300.00|Charles Hall|Philadelphia, PA 19143|
|$300.00|Shayonne D. Burrell|Darby, PA 19023|
|$300.00|Takeitha Lawson|Cincinnati, OH 45230|
|$300.00|Andrew P Brazington|Philadelphia, PA 19104|
|$300.00|Ida Wilson|Philadelphia, PA 19143|
|$300.00|Abigail Cohen|Houston, TX 77002|
|$300.00|Leonard Bonarek|Philadelphia, PA 19143|
|$300.00|Bryan Hanes|Philadelphia, PA 19104|
|$300.00|William Rhodes|Haverford, PA 19041|
|$300.00|Maureen Tate|Philadelphia, PA 19143|
|$300.00|James Donley|Philadelphia, PA 19143|
|$300.00|Jamie Gauthier|Philadelphia, PA 19143|
|$300.00|Laura McHugh|Philadelphia, PA 19147|
|$280.00|Michelle Holmes|Philadelphia, PA 19153|
|$250.00|Char Magaro|Enola, PA 17025|
|$250.00|George Poulin|Philadelphia, PA 19104|
|$250.00|Carol Eicher|Naples, FL 34108|
|$250.00|Adam Rom|Philadelphia, PA 19143|
|$250.00|Dana Hirschenbaum|Philadelphia, PA 19123|
|$250.00|Angela Curry|Philadelphia, PA 19104|
|$250.00|Cari Bender|Bryn Mawr, PA 19010|
|$250.00|Ellen Farber|Philadelphia, PA 19103|
|$250.00|Deborah Cuffy|Williamsburg, VA 23185|
|$250.00|Brian Ratigan|Philadelphia, PA 19143|
|$250.00|Ryan Spak|Philadelphia, PA 19143|
|$250.00|Marcus Iannozzi|Philadelphia, PA 19146|
|$250.00|Teresa Gillen|Philadelphia, PA 19146|
|$250.00|Timothy Horan|West Chester, PA 19380|
|$250.00|JOE MANKO|Philadelphia, PA 19146|
|$250.00|Hudson Cuffy|Melbourne, FL 32934|
|$250.00|David Nicklin|Philadelphia, PA 19143|
|$250.00|Mark Dichter|Philadelphia, PA 19107|
|$250.00|Don Webster|Philadelphia, PA 19104|
|$250.00|Melani Lamond|Philadelphia, PA 19143|
|$250.00|Riffat Chughtai|Murrysville, PA 15668|
|$250.00|Frank Innes|Philadelphia, PA 19143|
|$250.00|Carmen Lennon|Philadelphia, PA 19103|
|$250.00|Andre Gonsalves|Upper Marlboro, MD 20774|
|$250.00|Gale Branch-Alexander|Mullica Hill, NJ 08062|
|$250.00|Alain Joinville|Philadelphia, PA 19130|
|$250.00|Sean Dwyer|Philadelphia, PA 19104|
|$250.00|Marjorie Cuffy|Melbourne, FL 32934|
|$250.00|Martie Bernicker|Villanova, PA 19085|
|$250.00|Joseph Shapiro|Philadelphia, PA 19143|
|$250.00|Joshua McNeil|Philadelphia, PA 19143|
|$250.00|Kirsti Bracali|Philadelphia, PA 19143|
|$250.00|Carolyn Johnson|Philadelphia, PA 19107|
|$250.00|David Mandell|Philadelphia, PA 19143|
|$250.00|Jo-Ann Verrier|Philadelphia, PA 19143|
|$250.00|Corrine Wilson-Upshur|Smyrna, DE 19977|
|$250.00|Nancy Moses|Philadelphia, PA 19106|
|$250.00|Steven Wilson|Allen, TX 75013|
|$250.00|Mark Mendenhall|Philadelphia, PA 19143|
|$250.00|Paul Marcus|Elkins Park, PA 19027|
|$250.00|Paulette Greenwell|Philadelphia, PA 19104|
|$250.00|Sarah Clark Stuart|Philadelphia, PA 19103|
|$240.00|Marcie J Wood|Philadelphia, PA 19143|
|$200.00|Rose A Cheney|Philadelphia, PA 19147|
|$200.00|Michael Greenle|Philadelphia, PA 19125|
|$200.00|Andrew Toy|Philadelphia, PA 19103|
|$200.00|Cheryl Solomon|Philadelphia, PA 19151|
|$200.00|Colleen Thomas-Phillip|Wynnewood, PA 19096|
|$200.00|Ana Yoder|Philadelphia, PA 19143|
|$200.00|Mary Brown|Roslyn, PA 19001|
|$200.00|Alixandra Howard|Murrieta, CA 92562|
|$200.00|Tayyib Smith|Philadelphia, PA 19103|
|$200.00|Michael Scales|Sewell, NJ 08080|
|$200.00|Ernie Branch|Sewell, NJ 08080|
|$200.00|Malyka Rosanne Thornton Sankofa|Philadelphia, PA 19143|
|$200.00|Lauren Leatherbarrow|Philadelphia, PA 19104|
|$200.00|Robin Dominick|Philadelphia, PA 19104|
|$200.00|Horace Patterson|Philadelphia, PA 19139|
|$200.00|Dwayne Wharton|Philadelphia, PA 19119|
|$200.00|Carol Adams|Philadelphia, PA 19119|
|$200.00|Keith Cox|Philadelphia, PA 19143|
|$200.00|Margie Politzer|Philadelphia, PA 19143|
|$200.00|Nicholas Cernansky|Philadelphia, PA 19143|
|$200.00|Bruce McCullough|Philadelphia, PA 19143|
|$200.00|Susan Best|Philadelphia, PA 19143|
|$200.00|Jeanine C Avery|Philadelphia, PA 19130|
|$200.00|Mary Allegra|Philadelphia, PA 19143|
|$200.00|Alex Urevick-Ackelsberg|Philadelphia, PA 19107|
|$200.00|Jonathan McKamey|Philadelphia, PA 19143|
|$200.00|Jessica Moore|Philadelphia, PA 19143|
|$200.00|Sarah Sturtevant|Philadelphia, PA 19146|
|$180.00|Russell Moritz|Philadelphia, PA 19143|
|$175.00|Evette Banfield|Washington, DC 20011|
|$175.00|Jerene Good|Philadelphia, PA 19143|
|$175.00|Alissa Weiss|Philadelphia, PA 19145|
|$170.00|Estie John|Yeadon, PA 19050|
|$165.00|Jennifer Loustau|Phila, PA 19143|
|$160.00|MYRIAM SIFTAR|Philadelphia, PA 19143|
|$150.00|Richard King|Philadelphia, PA 19143|
|$150.00|Penny Ordway|Ardmore, PA 19003|
|$150.00|Dale Cuffy|Los Angeles, CA 90038|
|$150.00|Curtis J. Gregory|Philadelphia, PA 19151|
|$150.00|Maria Gauthier|Philadelphia, PA 19135|
|$150.00|Lauren Hansen-Flaschen|Philadelphia, PA 19143|
|$150.00|Larissa Mogano|Philadelphia, PA 19143|
|$150.00|Susan Callanen|Philadelphia, PA 19146|
|$150.00|Nancy Geryk|Philadelphia, PA 19143|
|$150.00|Nancy J Lanham|Philadelphia, PA 19106|
|$150.00|Kevin James|Newark, DE 19702|
|$150.00|David Hincher|Philadelphia, PA 19143|
|$150.00|Angelo Perryman|Cherry Hill, NJ 08034|
|$150.00|Matthew Stepp|Philadelphia, PA 19145|
|$150.00|Sandy Tribendis|Charlottesville, VA 22911|
|$150.00|Deborah Knight|Bear, DE 19701|
|$150.00|Randall Steketee|Ridgewood, NJ 07450|
|$150.00|Abby Thaker|Philadelphia, PA 19104|
|$150.00|Patrick G Colgan|Philadelphia, PA 19104|
|$150.00|Antoine Cruel|Philadelphia, PA 19143|
|$150.00|Margaret Wise|Philadelphia, PA 19104|
|$150.00|Erin Trent Johnson|Philadelphia, PA 19146|
|$150.00|William Tung|Philadelphia, PA 19143|
|$150.00|Ashley Merritt|Elkton, MD 21921|
|$150.00|Katrina High|Norristown, PA 19401|
|$150.00|Shannon Lawson|Cincinnati, OH 45230|
|$150.00|Carolyn T Adams|Philadelphia, PA 19144|
|$150.00|Leah Finnegan|Philadelphia, PA 19143|
|$150.00|Jessica Lowenthal|Philadelphia, PA 19143|
|$150.00|Mia Farrell|Norristown, PA 19401|
|$150.00|Abu R Rahman|Newtown Square, PA 19073|
|$150.00|Barbara Dwyer|Havertown, PA 19083|
|$150.00|Alisa Branch|Turnersville, NJ 08012|
|$125.00|Bonnie Eisenfeld|Philadelphia, PA 19103|
|$125.00|Joseph J Navitsky|Philadelphia, PA 19139|
|$125.00|Marlon A Cuffy|New Orleans, LA 70122|
|$125.00|Aisha Coulson-Walters|Battle Creek, MI 49015|
|$125.00|Richard Guffanti|Philadelphia, PA 19104|
|$125.00|Elyana Tarlow|Philadelphia, PA 19143|
|$120.00|Rita Gonzalves|Philadelphia, PA 19143|
|$120.00|Jacqui Bowman|Philadelphia, PA 19104|
|$120.00|Nina Liou|Philadelphia, PA 19130|
|$100.00|Rob Maliff|Philadelphia, PA 19104|
|$100.00|Phoebe Wood|Philadelphia, PA 19143|
|$100.00|Scott R Geryk|Philadelphia, PA 19143|
|$100.00|Paula Weiss|Philadelphia, PA 19115|
|$100.00|Lindsay Gilmour|Philadelphia, PA 19119|
|$100.00|Mabel Brazington|Yeadon, PA 19050|
|$100.00|Robert P Thomas|Philadelphia, PA 19104|
|$100.00|Yvonne B. Haskins|Philadelphia, PA 19119|
|$100.00|Elana Benamy|Philadelphia, PA 19126|
|$100.00|Lisa Stein|New York, NY 10128|
|$100.00|April Solomon|Drexel Hill, PA 19026|
|$100.00|Leigh Goldenberg|Philadelphia, PA 19147|
|$100.00|Leslie Thaxton|Parkville, MD 21234|
|$100.00|Aaron Wunsch|Philadelphia, PA 19104|
|$100.00|Layla Doman|Washington, DC 20001|
|$100.00|Randy Belin|Riverside, NJ 08075|
|$100.00|Russell Meddin|Philadelphia, PA 19103|
|$100.00|Deann B Mills|Philadelphia, PA 19104|
|$100.00|Madeleine Helmer|Philadelphia, PA 19146|
|$100.00|Sara Narva|Philadelphia, PA 19143|
|$100.00|Carrie Rathmann|Philadelphia, PA 19143|
|$100.00|Marisa Guerin|Philadelphia, PA 19143|
|$100.00|Alfred Airone|Philadelphia, PA 19139|
|$100.00|Nancy A Drye|Philadelphia, PA 19104|
|$100.00|Lisa McDonald Hanes|Philadelphia, PA 19104|
|$100.00|Sharene Azimi|Bernardsville, NJ 07924|
|$100.00|Jose Lopez|Philadelphia, PA 19121|
|$100.00|Sharomine Martin|Ardmore, PA 19003|
|$100.00|Darren Burrell|Darby, PA 19023|
|$100.00|James Burnett|Wilmington, DE 19804|
|$100.00|Katherine A Dowdell|Philadelphia, PA 19143|
|$100.00|Paul R Steinke|Philadelphia, PA 19143|
|$100.00|Magali Larson|Philadelphia, PA 19104|
|$100.00|Carrie Young|Philadelphia, PA 19119|
|$100.00|Lillian Marsh|Philadelphia, PA 19104|
|$100.00|Julie Scott|Philadelphia, PA 19143|
|$100.00|Angel Celeste St. Jean|Baltimore, MD 21217|
|$100.00|Andrew C. Wheeler|Philadelphia, PA 19143|
|$100.00|Christina Rosan|Philadelphia, PA 19104|
|$100.00|Jessica Humphries|Philadelphia, PA 19143|
|$100.00|Kyvha Cuffy-Curtis|Philadelphia, PA 19135|
|$100.00|Michael Froehlich|Philadelphia, PA 19143|
|$100.00|Ann Karlen|Philadelphia, PA 19143|
|$100.00|Rohan Branch|Long Beach, MS 39560|
|$100.00|Marina Isakowitz|Philadelphia, PA 19143|
|$100.00|Chad Jackson|Philadelphia, PA 19143|
|$100.00|Lisa Gauthier|Philadelphia, PA 19115|
|$100.00|Velte Jolly|Philadelphia, PA 19131|
|$100.00|Mark Birdsall|Philadelphia, PA 19119|
|$100.00|Tom Clemonn|Philadelphia, PA 19107|
|$100.00|Elaine Simon|Philadelphia, PA 19104|
|$100.00|Zenzi Reeves|Swedesboro, NJ 08085|
|$100.00|Ezra Wolfe|Philadelphia, PA 19143|
|$100.00|Natalie Dohrmann|Philadelphia, PA 19104|
|$100.00|Adrian Mair|Atlanta, GA 30308|
|$100.00|Denise Lynne Goren|Philadelphia, PA 19103|
|$100.00|Lauren Cliggitt|Philadelphia, PA 19143|
|$100.00|Christopher Mejia-Smith|Philadelphia, PA 19143|
|$100.00|Nancy Petersmeyer|Philadelphia, PA 19103|
|$100.00|Fred Wolfe Sr|Philadelphia, PA 19143|
|$100.00|Natalie W. Nixon|Philadelphia, PA 19119|
|$100.00|Linshuang Lu|Philadelphia, PA 19143|
|$100.00|Ellen Ryan|Philadelphia, PA 19146|
|$100.00|Alex Dews|Philadelphia, PA 19119|
|$100.00|Mariya Alexandra Khandros|Philadelphia, PA 19143|
|$100.00|John Barber|Philadelphia, PA 19143|
|$100.00|John Hogan|Philadelphia, PA 19131|
|$100.00|Kathleen Diller|Philadelphia, PA 19143|
|$100.00|Aesha Rasheedah West|Yeadon, PA 19050|
|$100.00|Sean Dorn|Philadelphia, PA 19143|
|$100.00|Jessica Levy|Philadelphia, PA 19143|
|$100.00|Stephanie Wein|Philadelphia, PA 19104|
|$100.00|Jamie E Kudera|Philadelphia, PA 19143|
|$100.00|Jacqueline Buhn|Philadelphia, PA 19103|
|$100.00|Dorothy Welch Berlind|Philadelphia, PA 19143|
|$100.00|Aasima Ali|Philadelphia, PA 19152|
|$100.00|Virginia Maksymowicz|Philadelphia, PA 19104|
|$100.00|Richard A Stasiorowski|Philadelphia, PA 19104|
|$100.00|Lauren M Vidas|Philadelphia, PA 19146|
|$100.00|Sam Neubardt|Philadelphia, PA 19104|
|$100.00|Rhonda West-Haynes|Coatesville, PA 19320|
|$100.00|Ann Glover|Philadelphia, PA 19150|
|$100.00|William Fleming|Philadelphia, PA 19146|
|$100.00|Hanley Bodek|Philadelphia, PA 19104|
|$100.00|Nancy Lisagor|Philadelphia, PA 19103|
|$100.00|Daniel Stern|Penn Valley, PA 19072|
|$100.00|Keenan Washington|Lansdale, PA 19446|
|$100.00|Robert Walther|Philadelphia, PA 19143|
|$100.00|Roberta Iversen|Swarthmore, PA 19081|
|$100.00|Sharon Gallagher|Philadelphia, PA 19103|
|$100.00|Sharon Barr|Philadelphia, PA 19119|
|$100.00|JANET STOTLAND|Philadelphia, PA 19104|
|$100.00|Margaret Sadler|Philadelphia, PA 19129|
|$100.00|Mary Goldman|Philadelphia, PA 19104|
|$100.00|Robert Cheetham|Philadelphia, PA 19130|
|$100.00|Jane Slusser|Philadelphia, PA 19145|
|$100.00|Dan O?Brien|Philadelphia, PA 19143|
|$100.00|Antonio Antunes|Philadelphia, PA 19146|
|$100.00|Rebecca A Wright|Philadelphia, PA 19143|
|$100.00|Susanna Gilbertson|Philadelphia, PA 19143|
|$100.00|Keisha Mowatt|Philadelphia, PA 19143|
|$100.00|Elise Drake|Philadelphia, PA 19102|
|$100.00|Jodi James Cheeks|Yeadon, PA 19050|
|$100.00|Carol Dubie|Philadelphia, PA 19143|
|$100.00|Irene Lin|Philadelphia, PA 19104|
|$100.00|David Jonas|Philadelphia, PA 19143|
|$100.00|Christine Knapp|Philadelphia, PA 19147|
|$100.00|Michael Lehman|Philadelphia, PA 19104|
|$100.00|Mary-Knight Bell Young|Philadelphia, PA 19143|
|$100.00|Daniel Levinthal|Philadelphia, PA 19103|
|$100.00|Christopher Hershberger Esh|Philadelphia, PA 19143|
|$100.00|Craig Griggs|Atlanta, GA 30326|
|$100.00|Khalif Stripling|Chatsworth, CA 91311|
|$100.00|Cecily Kihn|Philadelphia, PA 19103|
|$100.00|Elizabeth F Campion|Philadelphia, PA 19143|
|$100.00|Marisa Waxman|Philadelphia, PA 19146|
|$100.00|Joanne Aitken|Philadelphia, PA 19143|
|$100.00|Tonya Andrews|Philadelphia, PA 19143|
|$100.00|Erin McLeary|Philadelphia, PA 19143|
|$100.00|Susan Felker|Philadelphia, PA 19146|
|$100.00|Andrew Sharp|Philadelphia, PA 19146|
|$100.00|Francesca Engel|Philadelphia, PA 19143|
|$100.00|Shayna and Frederick Harvey|Drexel Hill, PA 19026|
|$100.00|Ferdinand Irizarry|Elkins Park, PA 19027|
|$100.00|Kenneth Steif|Philadelphia, PA 19143|
|$100.00|Jackie McCrea|Philadelphia, PA 19104|
|$100.00|Cara Ferrentino|Philadelphia, PA 19119|
|$100.00|Wanda Lewis|Philadelphia, PA 19101|
|$100.00|Lucy Strackhouse|Meadowbrook, PA 19046|
|$100.00|Danielle DiLeo Kim|Philadelphia, PA 19130|
|$100.00|Lisa Weidman|Philadelphia, PA 19139|
|$100.00|Kris Mellor Soffa|Phila, PA 19128|
|$100.00|Elizabeth Guman|Exton, PA 19341|
|$100.00|Lee Meinicke|Ambler, PA 19002|
|$100.00|Colin Hammar|Philadelphia, PA 19143|
|$99.98|Susanne Flood|Norristown, PA 19401|
|$99.98|George Kay|Drexel Hill, PA 19026|
|$99.98|Soneyet Muhammad|Philadelphia, PA 19146|
|$85.00|Kim Andrews|Merion Station, PA 19066|
|$80.00|Deborah Gross-Zuchman|Philadelphia, PA 19143|
|$75.00|David Lane Fisher|Philadelphia, PA 19104|
|$75.00|Joel Steiker|Philadelphia, PA 19143|
|$75.00|Emily Yoder|Somerville, MA 02145|
|$75.00|Heather Blakeslee|Philadelphia, PA 19147|
|$75.00|Brandon Alcorn|Philadelphia, PA 19143|
|$75.00|Ari Miller|Philadelphia, PA 19139|
|$75.00|Charlette Caldwell|Philadelphia, PA 19139|
|$75.00|Richard G Freeman|Philadelphia, PA 19143|
|$75.00|James Kurtz|Philadelphia, PA 19143|
|$60.00|Trudy Watt|Philadelphia, PA 19143|
|$60.00|William Foley|Glenside, PA 19038|
|$60.00|Michael Clark|Philadelphia, PA 19143|
|$60.00|Arwin Thomasson|Philadelphia, PA 19143|
|$55.00|Stephen DeLuca|Philadelphia, PA 19139|
|$51.00|Joseph Corrigan|Philadelphia, PA 19146|


