
import csv
import random

# quick and dirty dot density in a WGS84 grid

DOT_AMT = 50  # dollar amount of each dot

RANDOMIZE_RESOLUTION = 0.012
RANDOMIZE_RESOLUTION = 0.006

def randomize_value(val):
#    return float(val) + (RANDOMIZE_RESOLUTION * (random.random() - 0.5))
    val = float(val)
    if val < 0:
        flip = True
        val *= -1
    else:
        flip = False
    val /= RANDOMIZE_RESOLUTION
    val = int(val)
    val = float(val) + random.random()
    val *= RANDOMIZE_RESOLUTION
    if flip:
        val *= -1
    return val


with open('geo.tsv', 'rb') as f:
    with open('dotdensity.tsv', 'w') as out:
        out.write('candidate\tx\ty\n')
        for row in csv.DictReader(f, delimiter='\t'):
            for x in range(int((float(row['amount']) + (DOT_AMT / 2)) / DOT_AMT)):
                randx, randy = randomize_value(row['x']), randomize_value(row['y'])
                candidate = 'Blackwell' if 'blackwell' in row['candidate'].lower() else 'Gauthier'
                out.write('{}\t{}\t{}\n'.format(
                    candidate,
                    randx,
                    randy
                ))
