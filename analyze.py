
# https://apps.phila.gov/campaign-finance/search/all

import csv
import pprint
import locale


GEOCODE = True

if GEOCODE:
    import censusgeocode


entries = []

def process_record(record):
    if 'Selander' in row['EntityName']:
        row['EntityCity'] = 'Chadds Ford'
        # this city field was incorrectly entered in second address line
    if row['EntityName'] == 'PFT COMMITTEE TO SUPPORT':
        row['EntityName'] = 'PFT Committee to Support Public Education'
    if row['EntityName'] == 'SHEET ME47TAL WORKERS LOCAL 19':
        row['EntityName'] = 'Sheet Metal Workers Union Local 19'
        row['EntityAddressLine1'] = '1301 South Columbus Boulevard' #  "South" was abbreviated
                                                                    #  in one record
    if row['EntityAddressLine1'] == '665 N. Broad Street, 3rd Floor':
        row['EntityAddressLine1'] = '665 N. BROAD ST'
        row['EntityName'] = 'Laborers District Council (PAC)'  # misspelled in data
    return {
        'Campaign': row['FilerName'],
        'Contributor': row['EntityName'],
        'Amount': float(row['Amount']),
        'Address1': row['EntityAddressLine1'],
        'Address2': row['EntityAddressLine2'],
        'City': row['EntityCity'],
        'Zip': row['EntityZip'],
        'State': row['EntityState'],
        'hash': (row['EntityName'].upper().translate(None, ',.() '),
                 row['EntityAddressLine1'].upper().translate(None, ',. '))
    }

def geocode(record):
    try:
        address = '{}, {}, {} {}'.format(
            record['Address1'],
            record['City'],
            record['State'],
            record['Zip'][:5]
        )
        result = censusgeocode.onelineaddress(address)
        print(address, result[0]['coordinates'])
        record['Location'] = result[0]['coordinates']
    except IndexError:
        print(address, '???')
    return record



def sum_by_contributor(records):
    summed = {}
    for record in records:
        if record['hash'] in summed:
            summed[record['hash']]['Amount'] += record['Amount']
        else:
            summed[record['hash']] = record.copy()
    return summed.values()

with open('Report-2018-1557161232566.txt', 'rb') as csvfile:
    reader = csv.DictReader(csvfile, delimiter='\t', quotechar='"')
    for n, row in enumerate(reader):
        if row['DocType'] is None or 'Schedule I ' not in row['DocType']:
            continue
        try:
            if row['Amount'] != '':  # blank records?
                entries.append(process_record(row))
        except Exception as e:
            # these exception blocks are used to find and correct malformed
            # rows
            if 'JANNIE' in row['FilerName'] or 'Jamie' in row['FilerName']:
                print(n, row)
                raise e

with open('Report-2019-1557153010790..csv', 'rb') as csvfile:
    reader = csv.DictReader(csvfile, quotechar='"')
    for n, row in enumerate(reader):
        if row['DocType'] is None or 'Schedule I ' not in row['DocType']:
            continue
        try:
            entries.append(process_record(row))
        except Exception as e:
            # these exception blocks are used to find and correct malformed
            # rows
            if 'JANNIE' in row['FilerName'] or 'Jamie' in row['FilerName']:
                print(n, row)
                raise e

gauthier = sum_by_contributor([x for x in entries
                               if x['Campaign'].upper() == 'JAMIE FOR WEST PHILLY'])
blackwell = sum_by_contributor([x for x in entries
                                if x['Campaign'].upper() == 'FRIENDS OF JANNIE L. BLACKWELL'])

gauthier.sort(key=lambda x: -x['Amount'])
blackwell.sort(key=lambda x: -x['Amount'])
locale.setlocale( locale.LC_ALL, '' )

print('## Donor counts by size of total donation')
print('|Amount|Blackwell|$|Gauthier|$|')
print('|:--|:--|:--|:--|:--|')
for minimum, maximum in [(0, 200), (200.01, 400), (400.01, 800), (800.01, 1600), (1600.01, 3200),
                         (3200.01, 6400), (6400.01, 12800), (12800.01, 25600)]:
    print('|{}-{}|{}|{}|{}|{}|'.format(
        locale.currency(minimum),
        locale.currency(maximum),
        len([x for x in blackwell if x['Amount'] >= minimum and x['Amount'] <= maximum]),
        locale.currency(sum(x['Amount'] for x in blackwell if x['Amount'] >= minimum and x['Amount'] <= maximum)),
        len([x for x in gauthier if x['Amount'] >= minimum and x['Amount'] <= maximum]),
        locale.currency(sum(x['Amount'] for x in gauthier if x['Amount'] >= minimum and x['Amount'] <= maximum))
    ))
print('|Total|{}|{}|{}|{}|'.format(
    len(blackwell),
    locale.currency(sum(x['Amount'] for x in blackwell)),
    len(gauthier),
    locale.currency(sum(x['Amount'] for x in gauthier))
))

for name, data in [('Jannie Blackwell', blackwell), ('Jamie Gauthier', gauthier)]:
    print("## {}\n".format(name))
    total = sum(x['Amount'] for x in data)
    print('{} donors contributing {} in total in 2018 + 2019\n\n'.format(
        len(data),
        locale.currency(total)
    ))
    print('|Amount|Name|City, Zip|')
    print('|:--|:--|:--|')
    for contributor in data:
        print("|{}|{}|{}, {} {}|".format(
            locale.currency(contributor['Amount']),
            contributor['Contributor'],
            contributor['City'],
            contributor['State'],
            contributor['Zip'][:5]
        ))
    print('\n')

if GEOCODE:
    for record in blackwell + gauthier:
        geocode(record)
    with open('geo.tsv', 'w') as tsvfile:
        tsvfile.write('candidate\tamount\tx\ty\tname\taddress\n')
        for record in [r for r in blackwell + gauthier if 'Location' in r]:
            tsvfile.write('{}\t{}\t{}\t{}\t{}\t{}\n'.format(
                record['Campaign'],
                record['Amount'],
                record['Location']['x'],
                record['Location']['y'],
                record['Contributor'],
                '{}, {}, {} {}'.format(
                    record['Address1'],
                    record['City'],
                    record['State'],
                    record['Zip'][:5]
                )
            ))
